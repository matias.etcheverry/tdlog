//MultiArray<double, 3> hessian(Contraintes contraintes, Vector<double> x){
//    //H[i::] is the hessian of c_i
//    MultiArray<double, 3> H(contraintes.m, contraintes.n, contraintes.n);
//    for (int k=0; k<contraintes.m; k++){
//        Derivee* der = new Derivee(contraintes.c[k]);
//        for (int i=0; i<contraintes.n; i++){
//            for (int j=i; j<contraintes.n; j++){
//                double derivee_ij = der->operator()(x, i, j);
//                H(k, i, j) = derivee_ij;
//                H(k, j, i) = derivee_ij;
//            }
//        }
//    }
//    return H;
//}

//void fill_XZ(Matrix<double>& X, Matrix<double>& Z, Vector<double> x, double nu){
//    X.fill(0);
//    Z.fill(0);
//    X.setDiagonal(x);
//    for (int i=0; i<x.size(); i++){
//        if (x[i] != 0)
//            Z(i, i) = nu/x[i];
//    }
//}

//double compute_alpha(Fonction* f, Vector<double> x, Vector<double> dx){
//    double alpha = 1;
//    int i = 0;
//    double fx = f->operator()(x);
//    while (f->operator()(x+alpha*dx)>fx && i<10){
//        i++;
//        alpha*=0.10;
//    }
//    return alpha;
//}
