<!DOCTYPE html>
<html>

<head>
    <title>Display Solution</title>
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
    <?php
    $out = fopen("output.txt", "r");
    $number_variable = '';
    $number_step = '';
    $step_data = array();
    $f0 = '';
    $objective_value = '';
    $variable_value = '';
    $count = 0;
    while (!feof($out) && $count < 6) {
        $line = explode(" ", trim(fgets($out)));
        if ($count == 0) {
            $number_variable = $line[2];
        } elseif ($count == 1) {
            $number_step = $line[1];
        } elseif ($count == 3) {
            $variable_value = array_slice ($line, 1);
        } elseif ($count == 4) {
            $f0 = $line[1];
        } elseif ($count == 5) {
            $objective_value = $line[1];
        }
        $count++;
    }
    $int = fopen("input.txt", "r");
    $number_variable = '';
    $name = '';
    $objective = array();
    $number_constraint = '';
    $constraints = array();
    $inequality_constraint = array();
    $feasible_solution = '';
    $linear_f = false;
    $linear_c = false;
    $count = 0;
    while (!feof($int)) {
        $line = trim(fgets($int));
        if ($count == 0) {
            $number_variable = explode(" ", $line)[0];
        } elseif ($count == 1) {
            $name = explode(" ", $line);
        } elseif ($count == 2) {
            $feasible_solution = explode(" ", trim(fgets($int)));
            $count++;
        } elseif ($count == 4) {
            $linear_f = (bool)(explode(" ", $line)[1][0] == "l");
            $objective = explode(" ", trim(fgets($int)));
            $count++;
        } elseif ($count == 6) {
            $linear_c = (bool)(explode(" ", $line)[1][0] == "l");
            $number_constraint = intval(explode(" ", trim(fgets($int)))[0]);
            $count++;
            for ($i = 0; $i < $number_constraint; $i++) {
                $line = explode(" ", trim(fgets($int)));
                $line = array("c" => array_slice($line, 0, $number_variable), "b" => $line[$number_variable]);
                array_push($constraints, $line);
                $count++;
            }
        } elseif ($count > 8 + $number_constraint and strlen($line) > 0) {
            array_push($inequality_constraint, $line);
        }
        $count++;
    }
    ?>
    <br />
    <div class="container">
        <h2>
            Affichage des résultats
        </h2>
        <div class="row" style="margin-left: 5px">
            <h2>
                Valeurs
            </h2>
            <p>On donne la possibilité de tracer le "graphe 1" qui donne l'évolution d'une variable à chaque itération.
                L'option associée dans le tableau permet de sélectionner les variables à visualiser.</p>
            <form method="post" name="draw">
                <div class="row">
                    <div class="col-md-6">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Variable</th>
                                    <th scope="col">Valeur</th>
                                    <th scope="col">Graphe 1</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                echo
                                    "<tr>
                                <th scope='row'>0</th>
                                <td>Fonction objective f(x)</td>
                                <td>" . $objective_value . "</td>
                                <td>";
                                echo "<input type='checkbox' class='form-check-input' name='objective_function' value='0' >
                            </tr>";
                                for ($i = 0; $i < $number_variable; $i++) {
                                    echo
                                        "<tr>
                                    <th scope='row'>" . strval($i) + 1 . "</th>
                                    <td>" . $name[$i] . "</td>
                                    <td>" . $variable_value[$i] . "</td>
                                    <td>";
                                    echo "<input type='checkbox' class='form-check-input' name=" . $name[$i] . " value=" . strval($i + 1) . ">
                                </tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <input type="submit" class="btn btn-lg" name="graphe1" value="Afficher le graphe 1.">
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-md-4">
                <form method="get" action="output.txt">
                    <button class="btn btn-lg">Télécharger les solutions
                        (output)</button>
                </form>
            </div>
            <div class="col-md-4">
                <form method="get" action="input.txt">
                    <button class="btn btn-lg">Télécharger les entrées
                        (input)</button>
                </form>
            </div>
        </div>

        <?php
        $x_index = array();
        if (isset($_POST["graphe1"])) {
            if (isset($_POST["objective_function"])){
                array_push($x_index, 0);
            }
            for ($i = 0; $i < $number_variable; $i++) {
                if (isset($_POST[$name[$i]])) {
                    array_push($x_index, strval($i+1));
                }
            }
            echo exec('C:\Users\matia\AppData\Local\Programs\Python\Python39\python.exe "cgi-bin/draw.py" "' . "f(x)," . implode(",", $name) . '" "' . "$f0," . implode(",", $feasible_solution) . '" "' . implode(",", $x_index) . '"');
            header("Location: display.php");
        }
        ?>
        <div class="row" style="margin-left: 5px">
            <h2>
                Résumé
            </h2>
            <div class="row">
                <div class="col-md-6">
                    <p><u>Fonction objective:</u>
                        <?php
                        if ($linear_f) {
                            $plus = false;
                            for ($i = 0; $i < $number_variable; $i++) {
                                if (floatval($objective[$i]) != 0) {
                                    if ($plus)
                                        echo " + ";
                                    echo $objective[$i] . $name[$i];
                                    $plus = true;
                                }
                            }
                        } else
                            echo $objective;
                        ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <p><u>Contraintes d'égalité:</u>
                        <?php
                        if ($linear_c) {
                            if (count($constraints) == 0)
                                echo "Aucune.";
                            else {
                                echo "</p><table class='table table-striped table-bordered'>
                                    <thead>
                                        <tr>
                                            <th scope='col'>#</th>
                                            <th scope='col' colspan='2''>Contrainte</th>
                                            <th scope='col'>Valeur</th>
                                        </tr>
                                    </thead>
                                    <tbody>";
                                for ($m = 0; $m < $number_constraint; $m++) {
                                    echo
                                        "<tr>
                                    <th scope='row'>" . strval($m) + 1 . "</th>
                                    <td>";
                                    $plus = false;
                                    for ($i = 0; $i < $number_variable; $i++) {
                                        if (floatval($constraints[$m]["c"][$i]) != 0) {
                                            if ($plus)
                                                echo " + ";
                                            echo $constraints[$m]["c"][$i] . $name[$i];
                                            $plus = true;
                                        }
                                    }
                                    echo "</td>";
                                    echo "<td style='text-align: center'> = </td>";
                                    echo "<td>" . $constraints[$m]["b"] . "</td>";
                                    echo "</tr>";
                                }
                                echo "</tbody>
                        </table>";
                            }
                        }
                        ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <p><u>Contraintes d'inégalité:</u>
                        <?php
                        if (count($inequality_constraint) == 0)
                            echo "Aucune.";
                        else {
                            echo "</p><table class='table table-striped table-bordered'>
                                <thead>
                                    <tr>
                                        <th scope='col'>#</th>
                                        <th scope='col' colspan='2'>Contrainte</th>
                                        <th scope='col'>Valeur</th>
                                    </tr>
                                </thead>
                                <tbody>";
                            for ($m = 0; $m < count($inequality_constraint); $m++) {
                                echo
                                    "<tr>
                                <th scope='row'>" . strval($m) + 1 . "</th>
                                <td>";
                                echo $inequality_constraint[$m];
                                echo "</td>";
                                echo "<td style='text-align: center'>≥</td>";
                                echo "<td> 0 </td>";
                                echo "</tr>";
                            }
                            echo "</tbody>
                        </table>";
                        }
                        ?>
                </div>
            </div>
        </div>
    </div>
</body>

</html>