#ifndef READING_H
#define READING_H

#include <Imagine/LinAlg.h>
#include <Imagine/Common.h>
#include<iostream>
using namespace std;
using namespace Imagine;

class Problem{
public:
    int n;
    int m;
    double nu;
    bool linear_f;
    bool linear_c;
    Vector<string> unknown;
    Vector<double> x0;
    Vector<double> f_lin;
    string f;
    Matrix<double> c_lin;
    Vector<double> b;
    Vector<string> c;
    string z;

    Vector<double> x;
    Vector<double> lambda;

    Problem(string data);

};


#endif // READING_H
