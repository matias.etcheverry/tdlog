#include "reading.h"
#include <iostream>
#include <cassert>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
#include <string>
using namespace std;


Problem::Problem(string data){
    nu = 10;
    string line;
    ifstream myfile ("C:/xampp/htdocs/PROJ_TDLOG2/"+data);
    int i=1;
    if (myfile.is_open()){
        while (getline(myfile,line)){
            if (i==1) {
                n=stoi(line);
                x.setSize(n);
                f_lin.setSize(n);
                unknown.setSize(n);
            }
            if (i==2){
                istringstream iss(line);
                for (int k=0; k<n; k++){
                    iss>>unknown[k];
                }
            }
            if (i==3){
                if (line=="x0"){
                    x0.setSize(n);
                    getline(myfile,line);
                    istringstream iss(line);
                    for (int k=0; k<n; k++){
                        iss>>x0[k];
                    }
                    getline(myfile,line);
                }
                if (line == "f l") linear_f=true;
                else linear_f=false;
            }
            if (i==4){
                if (linear_f){
                    istringstream iss(line);
                    for (int k=0; k<n; k++){
                        iss>>f_lin[k];
                    }
                }
                else f = line;
            }
            if (i==5){
                if (line == "c l") linear_c=true;
                else linear_c=false;
            }
            if (i==6) {
                m=stoi(line);
                lambda.setSize(m);
                if (linear_c) {
                    c_lin.setSize(m, n);
                    b.setSize(m);
                }
                else c.setSize(m);
            }
            if (i>=7 && i<7+m){
                if (linear_c){
                    istringstream iss(line);
                    for (int k=0; k<n; k++){
                        iss>>c_lin(i-7, k);
                    }
                    iss>>b[i-7];
                }
                else c[i-7] = line;
            }
            if (i>7+m && !line.empty()){
                z+="log("+line+")+";
            }
            i++;
        }
        myfile.close();
        z = z.substr(0, z.size()-1);
    }
}


