#!C:\Users\matia\AppData\Local\Programs\Python\Python39\python.exe
import os, sys
import cgi
import cgitb
import plotly.graph_objs as go
import plotly as ply
import numpy as np

cgitb.enable()
cgitb.enable(display=0, logdir="C:/xampp/htdocs/PROJ_TDLOG2/logs")

stri = "Content-type: text/html; charset=utf-8\n\n"
stri += "<html><head><title>Répertoire local</title></head><body>"

form = cgi.FieldStorage()

arguments = cgi.FieldStorage()
stri += str(sys.argv[1])
stri += str(sys.argv[2]) + " -"
stri += str(sys.argv[3]) + "- "

names = sys.argv[1].split(',')
x0 = sys.argv[2].split(',')
x0 = [float(i) for i in x0]
x_index = sys.argv[3].split(',')
x_index = [int(i) for i in x_index]
stri += "\t" + str(names)
stri += str(x0)


def evolution_in_time(names, x0, x_index):
    y = [0]
    x_temp = []
    for i in x_index:
        x_temp.append(x0[i])
    x = [x_temp]
    count = 0
    with open("C:/xampp/htdocs/PROJ_TDLOG2/output.txt") as reader:
        for line in reader:
            line = line.split(" ")
            if count > 6:
                y.append(count + 1)
                x_temp = []
                for i in x_index:
                    x_temp.append(float(line[i + 1]))
                x.append(x_temp)
            count += 1
    x = np.array(x)
    data = []
    save = "C:/xampp/htdocs/PROJ_TDLOG2/figs/evolution_in_time_"
    title = "Evolution dans le temps de "
    for i in range(len(x_index)):
        color = list(np.random.random(3))
        trace = go.Scatter(x=y, y=x[:, i], mode='lines', name=names[x_index[i]])
        save += names[x_index[i]] + "_"
        title += names[x_index[i]] + ","
        data.append(trace)

    layout = go.Layout(
        title=dict(
            text=title[0:-1],
            font=dict(
                family='Courier New, monospace',
                size=24,
                color='#7f7f7f'
            )
        ),
        xaxis=dict(
            title='Temps',
            titlefont=dict(
                family='Courier New, monospace',
                size=18,
                color='#7f7f7f'
            )
        ),
    )
    fig = go.Figure(data=data, layout=layout)

    ply.offline.plot(fig, filename=save[0:-1] + ".html")


stri += "</body></html>"
# print(stri)
evolution_in_time(names, x0, x_index)
