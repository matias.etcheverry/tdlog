<?php
error_reporting(E_ALL); // check all type of errors
ini_set('display_errors',1);// display those errors 
session_start();

$error = '';
$number_variable = '';
$name = '';
$objective = '';
$number_constraint = '';
$constraints = '';
$inequality_constraint = '';
$feasible_solution = '';

function clean_text($string)
{
 $string = trim($string); #removes extrem spaces
//  $string = stripslashes($string); #removes \
//  $string = htmlspecialchars($string); #changes special chars like é
 return $string;
}

if(isset($_POST["submit"]))
{
    $name = clean_text($_POST["name"]);
    $number_variable = count(explode(" ", $name));
    $objective = clean_text($_POST["objective"]);
    $constraints = clean_text($_POST["constraints"]);
    $number_constraint = count(explode("\n", $constraints));
    $inequality_constraint = clean_text($_POST["inequality_constraint"]);
    $feasible_solution = clean_text($_POST["feasible_solution"]);
    $linear_f = $_POST["linear_objective"];
    $linear_c = $_POST["linear_constraint"];
    if ($linear_f == 1)
        $linear_f = "nl";
    else {
        $linear_f = "l";
    }
    if ($linear_c == 1)
        $linear_c = "nl";
    else {
        $linear_c = "l";
    }

        
//  if(empty($_POST["number_variable"]))
//  {
//     $_SESSION["errormsg"]='you must fill password';
//  }
//  else
//  {
//   $number_variable = clean_text($_POST["number_variable"]);
//   if(preg_match("/^\d+$/", $number_variable))
//   {
//     $_SESSION["errormsg"]='you must fill password';
//   }
//  }
//  if(empty($_POST["email"]))
//  {
//   $error .= '<p><label class="text-danger">Please Enter your Email</label></p>';
//  }
//  else
//  {
//   $email = clean_text($_POST["email"]);
//   if(!filter_var($email, FILTER_VALIDATE_EMAIL))
//   {
//    $error .= '<p><label class="text-danger">Invalid email format</label></p>';
//   }
//  }
//  if(empty($_POST["subject"]))
//  {
//   $error .= '<p><label class="text-danger">Subject is required</label></p>';
//  }
//  else
//  {
//   $subject = clean_text($_POST["subject"]);
//  }
//  if(empty($_POST["message"]))
//  {
//   $error .= '<p><label class="text-danger">Message is required</label></p>';
//  }
//  else
//  {
//   $message = clean_text($_POST["message"]);
//  }

 if($error == '')
 {
  $file_open = fopen("input.txt", "w");
  $form_data = array(
   $number_variable,
   $name,
   "x0",
   $feasible_solution,
   "f " . $linear_f,
   $objective,
   "c " . $linear_c,
   $number_constraint,
   $constraints,
   "ineq",
   $inequality_constraint,
  );
  fputs($file_open, implode("\n", $form_data)."\n");
  $error = '<label class="text-success">Thank you for contacting us</label>';
 }
}

header  ('Location: cgi-bin/TDLOG.cgi');
?>