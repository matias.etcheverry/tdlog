#ifndef MATRICE_H
#define MATRICE_H

#include <Imagine/LinAlg.h>
#include <Imagine/Common.h>
#include <iostream>
#include <muParser.h>
#include <muParserTest.h>
#include "reading.h"
using namespace std;
using namespace mu;
using namespace Imagine;

const double eps = 0.01;
const int step_number = 500;

class Fonction
{
public:
    int n;
    bool linear;
    Parser p;
    Vector<double> var;

    Fonction(){}
    Fonction(Problem pb);
    Fonction(int i, Problem pb);
    Fonction(int n, string operation, Vector<string> unknown);
    ~Fonction() {}
    void get_var();
    double operator()(Vector<double> x);
    void fill_jacobian(Vector<double> x);
    void fill_hessian(Vector<double> x);
};

class Contraintes
{
public:
    int m;
    int n;
    bool linear;
    Vector<Fonction*> c;
    Matrix<double> var;
    Vector<double> b; //var*x = b

    Contraintes(){}
    Contraintes(Problem p);
    ~Contraintes() {}
    Vector<double> operator()(Vector<double> x);
};

class Derivee: public Fonction{
    Fonction* integrale;
public:
    Derivee(Fonction* inte);
//    Fonction* clone();
    ~Derivee(){
        delete integrale;
    }
    double operator()(Vector<double> x, int k) const;
    double operator()(Vector<double> x, int k, int l) const;
    Derivee* derivee();
};


#endif // MATRICE_H
