# CMake generated Testfile for 
# Source directory: C:/xampp/htdocs/PROJ_TDLOG2/muparser
# Build directory: C:/xampp/htdocs/PROJ_TDLOG2/muparser
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
if("${CTEST_CONFIGURATION_TYPE}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
  add_test(ParserTest "C:/xampp/htdocs/PROJ_TDLOG2/muparser/Debug/t_ParserTest.exe")
  set_tests_properties(ParserTest PROPERTIES  _BACKTRACE_TRIPLES "C:/xampp/htdocs/PROJ_TDLOG2/muparser/CMakeLists.txt;175;add_test;C:/xampp/htdocs/PROJ_TDLOG2/muparser/CMakeLists.txt;0;")
elseif("${CTEST_CONFIGURATION_TYPE}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
  add_test(ParserTest "C:/xampp/htdocs/PROJ_TDLOG2/muparser/Release/t_ParserTest.exe")
  set_tests_properties(ParserTest PROPERTIES  _BACKTRACE_TRIPLES "C:/xampp/htdocs/PROJ_TDLOG2/muparser/CMakeLists.txt;175;add_test;C:/xampp/htdocs/PROJ_TDLOG2/muparser/CMakeLists.txt;0;")
elseif("${CTEST_CONFIGURATION_TYPE}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
  add_test(ParserTest "C:/xampp/htdocs/PROJ_TDLOG2/muparser/MinSizeRel/t_ParserTest.exe")
  set_tests_properties(ParserTest PROPERTIES  _BACKTRACE_TRIPLES "C:/xampp/htdocs/PROJ_TDLOG2/muparser/CMakeLists.txt;175;add_test;C:/xampp/htdocs/PROJ_TDLOG2/muparser/CMakeLists.txt;0;")
elseif("${CTEST_CONFIGURATION_TYPE}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
  add_test(ParserTest "C:/xampp/htdocs/PROJ_TDLOG2/muparser/RelWithDebInfo/t_ParserTest.exe")
  set_tests_properties(ParserTest PROPERTIES  _BACKTRACE_TRIPLES "C:/xampp/htdocs/PROJ_TDLOG2/muparser/CMakeLists.txt;175;add_test;C:/xampp/htdocs/PROJ_TDLOG2/muparser/CMakeLists.txt;0;")
else()
  add_test(ParserTest NOT_AVAILABLE)
endif()
