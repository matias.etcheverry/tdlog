#include <iostream>
#include <cassert>
#include <ctime>
#include <cmath>
#include <Imagine/LinAlg.h>
#include <Imagine/Common.h>
#include "muParser.h"
#include <fstream>
using namespace std;
using namespace mu;
using namespace Imagine;
#include "matrice.h"
#include "reading.h"


// TODO: hessian, jacobian part of fonctions
Vector<double> jacobian(Fonction* f, Vector<double> x){
    Derivee* der = new Derivee(f);
    Vector<double> j(f->n);
    for (int i=0; i<f->n; i++){
        j[i] = der->operator()(x, i);
    }
    return j;
}

Matrix<double> jacobian(Contraintes contraintes, Vector<double> x){
    //x size (m, n) with x.line[i] = coords in which we take the jacobian of c_i
    //J size (n, m) with J.col[i] = result of jacob(c_i)
    Matrix<double> J(contraintes.n, contraintes.m);
    for (int i=0; i<contraintes.m; i++){
        if (!contraintes.linear)
            J.setCol(i, jacobian(contraintes.c[i], x));
        else return transpose(contraintes.var);
    }
    return J;
}

Matrix<double> hessian(Fonction* f, Vector<double> x){
    Matrix<double> h(f->n, f->n);
    Derivee* der = new Derivee(f);
    for (int i=0; i<f->n; i++){
        for (int j=i; j<f->n; j++){
            double derivee_ij = der->operator()(x, i, j);
            h(i, j) = derivee_ij;
            h(j, i) = derivee_ij;
        }
    }
    return h;
}

double compute_alpha(Fonction* f, Vector<double> x, Vector<double> dx){
    double alpha = 1;
    int i = 0;
    double fx = f->operator()(x);
    while (f->operator()(x+alpha*dx)>fx && i<10){
        i++;
        alpha*=0.10;
    }
    return alpha;
}



void fill_AB(Matrix<double>& A, Vector<double>& B,
             Fonction* f, Contraintes c, Fonction* z, Problem pb){
    // TODO: operator hessian*lambda
    A.fill(0.0f);
    B.fill(0.0f);
    Matrix<double> W = - hessian(z, pb.x)*pb.nu;
    Matrix<double> C_grad(f->n, c.m);
    Vector<double> L_der(f->n);
    Vector<double> C_eval = c(pb.x);
    if (c.linear){
        C_grad = transpose(c.var);
        L_der = transpose(c.var)*pb.lambda;
    }
    else {
        C_grad = jacobian(c, pb.x);
        L_der = jacobian(c, pb.x)*pb.lambda;
        for (int i=0; i<c.m; i++){
            W += pb.lambda[i]*hessian(c.c[i], pb.x);
        }
    }
    if (f->linear){
        L_der += f->var;
    }
    else {
        L_der += jacobian(f, pb.x);
        W += hessian(f, pb.x);
    }


    for (int i=0; i<f->n; i++){
        for (int j=0; j<f->n; j++){
            A(i, j) = W(i, j);
        }
        for (int j=f->n; j<f->n+c.m; j++){
            A(i, j) = C_grad(i, j-f->n);
        }
        B[i] = -L_der[i];
    }
    for (int i=f->n; i<f->n+c.m; i++){
        for (int j=0; j<f->n; j++){
            A(i, j) = C_grad(j, i-f->n);
        }
        // TODO: generalise without for
        B[i] = -C_eval[i-f->n];
    }
}

void fill_AB_lesson(Matrix<double>& A, Vector<double>& B,
                    Fonction* f, Contraintes c, Fonction* z, Problem pb,
                    Vector<double> x){
    Matrix<double> W = hessian(f, x) + hessian(z, x)*pb.nu;
    A.fill(0.0f);
    B.fill(0.0f);
    Vector<double> L_der = jacobian(f, x) + jacobian(z, x)*pb.nu;
    // TODO: search for matrix to put right in
    for (int i=0; i<f->n; i++){
        for (int j=0; j<f->n; j++){
            A(i, j) = W(i, j);
        }
        for (int j=f->n; j<f->n+c.m; j++){
            A(i, j) = c.var(j-f->n, i);
        }
        B[i] = -L_der[i];
    }
    for (int i=f->n; i<f->n+c.m; i++){
        for (int j=0; j<f->n; j++){
            A(i, j) = c.var(i-f->n, j);
        }
        // TODO: generalise without for
    }
}



void solve(Fonction* f, Contraintes c, Fonction* z, Problem pb, bool write){
    Matrix<double> A(f->n+c.m, f->n+c.m);
    Vector<double> B(f->n+c.m);
    Vector<double> y(f->n+c.m);
    Vector<double> dx(f->n);
    double alpha;
    ofstream myfile;
    string str = "";
    int step = 0;
    if (write){
        myfile.open ("C:/xampp/htdocs/PROJ_TDLOG2/output.txt");
        // myfile.open("C:/Users/valentine/Documents/Ponts/TDLOG/output.txt");
        myfile << "mu" << " number_variables " << to_string(f->n) + "\n";
    }
    while(pb.nu>0.001){
        for (int i=0; i<10; i++){
            step++;
            //            fill_AB_lesson(A, B, f, c, z, pb, x);
            fill_AB(A, B, f, c, z, pb);
            y = linSolve(A,B);
            dx = y.getSubVect(0, f->n);
//            alpha = compute_alpha(f, pb.x, dx);
            alpha = 0.01;
            pb.x += alpha*dx;
            //            cout<<alpha<<endl;
            pb.lambda += alpha*y.getSubVect(f->n, c.m);
        }
        if (write){
            str += to_string(pb.nu) + " ";
            str += to_string(f->operator()(pb.x)) + " ";
            for (int i=0; i<pb.n; i++)
                str += to_string(pb.x[i]) + " ";
            str += "\n";
        }

        cout<<pb.x<<endl;
        pb.nu/=1.1;
    }
    if (write){
        myfile << "steps " << to_string(step) << endl;
        myfile << "x0 ";
        for (int i=0; i<pb.n; i++){
            myfile << to_string(pb.x0[i]) + " ";
        }
        myfile << endl;
        myfile << "xfinal ";
        for (int i=0; i<pb.n; i++){
            myfile << to_string(pb.x[i]) + " ";
        }
        myfile << endl;
        myfile << "f0 " << f->operator()(pb.x0) << endl;
        myfile << "ffinal " << f->operator()(pb.x) <<endl;
        myfile << "mu f(x) ";
        for (int i=0; i<pb.n; i++){
            myfile << pb.unknown[i] + " ";
        }
        myfile << endl;
        myfile << str;
        myfile.close();
    }
}

ostream& operator<<(ostream& str, const Problem pb){
    str<<"n variables: "<<pb.n<<endl;
    str<<pb.unknown<<endl;
    str<<"m constraints: "<<pb.m<<endl;
    str<<"nu: "<<pb.nu<<endl;
    if (pb.linear_f){
        str<<"F: "<<pb.f_lin<<endl;
    }
    else str<<"f: "<<pb.f<<endl;
    str<<"Barriere: "<<pb.z<<endl;
    if (pb.linear_c){
        str<<"c: "<<pb.c_lin<<endl;
        str<<"b: "<<pb.b<<endl;
    }
    else str<<"c: "<<pb.c<<endl;
    if (pb.x0.size()==pb.n) str<<"X0: "<<pb.x0<<endl;
    return str;
}

//int main()
//{
//    Problem pb("input.txt");
//    //Define objective function
//    Fonction* f = new Fonction(pb);
//    Fonction* z = new Fonction(f->n, pb.z, pb.unknown);

//    //Define constraints functions
//    Contraintes c(pb);

//    //Define vector solutions x
//    for (int i=0; i<pb.n; i++){
//        pb.x[i] = pb.x0[i];
//    }

//    //Define vector parameters lambda
//    for (int i=0; i<pb.m; i++){
//        pb.lambda[i] = i+1;
//    }
//    //Define step size
//    clock_t begin = clock();
//    solve(f, c, z, pb, true);
//    clock_t end = clock();
//    double elapsed = double(end - begin) / CLOCKS_PER_SEC;
//    cout<<"Content-Type:text/html\r\n\r\n"<<endl;
//    cout<<"<html><head><title>Affichage des résultats</title></head><body>"<<endl;
//    cout<<"-----------------------"<<endl;
//    cout<<pb<<endl;
//    cout<<"-----------------------"<<endl;
//    cout<<"X: "<<pb.x<<endl;
//    cout<<"f(x): "<<f->operator()(pb.x)<<endl;
//    cout<<"Lambda: "<<pb.lambda<<endl;
//    cout<<"Elapsed time "<<elapsed<<endl;
//    cout<<"</body></html>"<<endl;

//    return 0;
//}

int main()
{
    Problem pb("input.txt");
//    //Define objective function
    Fonction* f = new Fonction(pb);
    Fonction* z = new Fonction(f->n, pb.z, pb.unknown);

//    //Define constraints functions
    Contraintes c(pb);

    cout<<pb<<endl;
    //Define vector solutions x
    for (int i=0; i<pb.n; i++){
        pb.x[i] = pb.x0[i];
    }


//    Define vector parameters lambda
    for (int i=0; i<pb.m; i++){
        pb.lambda[i] = i+1;
    }

    //Define step size
    clock_t begin = clock();
    solve(f, c, z, pb, true);
    clock_t end = clock();
    double elapsed = double(end - begin) / CLOCKS_PER_SEC;

//    return 0;
}


