#include "matrice.h"
#include <iostream>
#include <cassert>
#include <cmath>
#include <string>
#include <muParserTest.h>
using namespace std;
using namespace mu;


Fonction::Fonction(Problem pb){
    n = pb.n;
    linear = pb.linear_f;
    if (linear){
        var.setSize(n);
        for (int i=0; i<n; i++){
            var[i] = pb.f_lin[i];
        }
    }
    else {
        var.setSize(n);
        var.fill(0);
        for (int i=0; i<n; i++){
            p.DefineVar(pb.unknown[i], &var[i]);
        }
        p.DefineConst("nu", pb.nu);
        p.SetExpr(pb.f);
    }
}

Fonction::Fonction(int i, Problem pb){
    n = pb.n;
    linear = false;
    var.setSize(n);
    var.fill(0);
    for (int i=0; i<n; i++){
        p.DefineVar(pb.unknown[i], &var[i]);
    }
    p.DefineConst("nu", pb.nu);
    p.SetExpr(pb.c[i]);
}

Fonction::Fonction(int k, string operation, Vector<string> unknown){
    n = k;
    linear = false;
    var.setSize(n);
    var.fill(0);
    for (int i=0; i<n; i++){
        p.DefineVar(unknown[i], &var[i]);
    }
    p.SetExpr(operation);
}

void Fonction::get_var(){
    varmap_type variables = p.GetVar();
    cout << "Number: " << (int)variables.size() << "\n";
    varmap_type::const_iterator item = variables.begin();
    for (; item!=variables.end(); ++item){
        cout << "Name: " << item->first << " Address: [0x" << item->second << "]\n";
    }
}

double Fonction::operator()(Vector<double> x){
    if (linear)
        return var*x;
    else {
        for (int i=0; i<n; i++){
            var[i] = x[i];
        }
        try{
            return p.Eval();
        }
        catch (mu::Parser::exception_type &e)
        {
            std::cout << e.GetMsg() << std::endl;
        }
    }
}


Contraintes::Contraintes(Problem pb){
    n = pb.n,
    m = pb.m;
    linear = pb.linear_c;
    if (linear){
        var.setSize(m, n);
        b.setSize(m);
        for (int i=0; i<m; i++){
            for (int k=0; k<n; k++){
                var(i, k) = pb.c_lin(i, k);
            }
            b[i] = pb.b[i];
        }
    }
    else {
        c.setSize(m);
        for (int i=0; i<m; i++){
            c[i] = new Fonction(i, pb);
        }
    }

}

Vector<double> Contraintes::operator()(Vector<double> x){
    if (linear) return var*x-b;
    else{
        Vector<double> result(m);
        for (int i=0; i<m; i++){
            result[i] = c[i]->operator()(x);
        }
        return result;
    }

}

Derivee::Derivee(Fonction* inte): Fonction(){
    integrale = inte;
}

Derivee* Derivee::derivee(){
    return new Derivee(this);
}

double Derivee::operator()(Vector<double> x, int k) const{
    Vector<double> x_up(integrale->n);
    Vector<double> x_down(integrale->n);
    for (int i=0; i<integrale->n; i++){
        x_up[i] = x[i];
        x_down[i] = x[i];
        if (i==k){
            x_up[i] += eps;
            x_down[i] -= eps;
        }
    }
    return (integrale->operator()(x_up) - integrale->operator()(x_down))/(2*eps);
}

double Derivee::operator()(Vector<double> x, int k, int l) const{
    Vector<double> x_up(integrale->n);
    Vector<double> x_down(integrale->n);
    for (int i=0; i<integrale->n; i++){
        x_up[i] = x[i];
        x_down[i] = x[i];
        if (i==k){
            x_up[i] += eps;
            x_down[i] -= eps;
        }
    }
    return (operator()(x_up, l) - operator()(x_down, l))/(2*eps);
}


//Trigo::Trigo(string function){
//    f = function;
//}

//Fonction* Trigo::clone(){
//    return new Trigo(*this);
//}

//Fonction* Trigo::derivee(){
//    Derivee* d = new Derivee(this);
//    return d;
//}

//double Trigo::operator()(double x) const{
//    if (f.compare("cos")==0) return cos(x);
//    else if (f.compare("sin")==0) return sin(x);
//    else if (f.compare("tan")==0) return tan(x);
//}

//double Trigo::inverse(double y){
//    double x0 = 1;
//    double xi = 0;
//    Derivee* derivee = new Derivee(this);
//    while (abs(xi-x0)>10e-5){
//        x0 = xi;
//        xi = x0 + (y-operator()(x0))/derivee->operator()(x0);
//    }
//    return xi;
//}

//void Trigo::print(){
//    cout<<"Fonction trigo :"<<f<<endl;
//}
